//
//  BleManager.h
//  Hex
//
//  Created by Shaun Wuth on 2018/08/01.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "BeaconManager.h"

@interface BleManager : NSObject<CBCentralManagerDelegate, BeaconManagerDelegate>

- (void) startScanning;
- (void) cleanup;
- (void) cleanup: (CBPeripheral *) peripheral;

@end
