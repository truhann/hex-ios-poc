//
//  BleController.m
//  Hex
//
//  Created by Shaun Wuth on 2018/08/01.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import "BleController.h"
#import "BleManager.h"
#import "BlePeripheral.h"
#import "BleControllerPrivate.h"

@interface BleController()

@property BleManager *bleManager;
@property BlePeripheral *blePeripheral;

@end

@implementation BleController

- (instancetype)init {
    if (self = [super init]) {
        [self setup];
    }
    
    return self;
}

- (void) setup {
    self.bleManager = [BleManager new];
    self.blePeripheral = [BlePeripheral new];
    
    NSLog(@"BleController Setup");
}

- (BlePeripheral *) getPeripheralDelegate {
    return self.blePeripheral;
}

- (BleManager *) getManagerDelegate {
    return self.bleManager;
}

@end
