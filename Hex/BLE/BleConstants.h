//
//  BleConstants.h
//  Hex
//
//  Created by Shaun Wuth on 2018/08/01.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#ifndef BleConstants_h
#define BleConstants_h

#define PACKET_SERVICE_UUID           @"00FF"
#define PACKET_CHARACTERISTIC_UUID_A  @"FF01"
#define PACKET_CHARACTERISTIC_UUID_B  @"FF02"

#define PACKET_LONG_LENGTH 128

#endif /* BleConstants_h */
