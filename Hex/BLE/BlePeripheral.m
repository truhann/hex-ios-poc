//
//  BlePeripheral.m
//  Hex
//
//  Created by Shaun Wuth on 2018/08/01.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import "BlePeripheral.h"
#import "BleController.h"
#import "BleControllerPrivate.h"
#import "BleConstants.h"
#import "DataHandler.h"
#import "DeviceModel.h"
#import "NotificationUtils.h"
#import "NSData+FastHex.h"
@interface BlePeripheral()

@property id<DataDelegate> dataDelegate;

@end

@implementation BlePeripheral

- (instancetype)init {
    if (self = [super init]) {
        [self setup];
    }
    return self;
}

- (void) setup {
    self.dataDelegate = [DataHandler new];
}

- (void)peripheralDidUpdateName:(CBPeripheral *)peripheral {
    
}

- (void)peripheral:(CBPeripheral *)peripheral didModifyServices:(NSArray<CBService *> *)invalidatedServices {
    
}

- (void)peripheral:(CBPeripheral *)peripheral didReadRSSI:(NSNumber *)RSSI error:(nullable NSError *)error {
    
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(nullable NSError *)error {
    
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    if (error) {
        NSLog(@"Error discovering services: %@", [error localizedDescription]);
        [[[[AppDelegate instance] bleController] getManagerDelegate] cleanup];
        return;
    }
    
    // Discover the characteristic we want...
    [NotificationUtils localNotification:@"Did Discover Services"];
    
    // Loop through the newly filled peripheral.services array, just in case there's more than one.
    for (CBService *service in peripheral.services) {
        [peripheral discoverCharacteristics:@[[CBUUID UUIDWithString:PACKET_CHARACTERISTIC_UUID_A],[CBUUID UUIDWithString:PACKET_CHARACTERISTIC_UUID_B]] forService:service];
    }
}


/** The Transfer characteristic was discovered.
 *  Once this has been found, we want to subscribe to it, which lets the peripheral know we want the data it contains
 */
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    // Deal with errors (if any)
    if (error) {
        NSLog(@"Error discovering characteristics: %@", [error localizedDescription]);
        [[[[AppDelegate instance] bleController] getManagerDelegate] cleanup];
        [[[[AppDelegate instance] bleController] getManagerDelegate] startScanning];
        return;
    }
    
    NSLog(@"Did Discover Characteristics");
    [NotificationUtils localNotification:@"Did Discover Characteristics"];
    
    // Again, we loop through the array, just in case.
    for (CBCharacteristic *characteristic in service.characteristics) {
        
        // And check if it's the right one
        BOOL isACharacteristic = [characteristic.UUID isEqual:[CBUUID UUIDWithString:PACKET_CHARACTERISTIC_UUID_A]];
        BOOL isBCharacteristic = [characteristic.UUID isEqual:[CBUUID UUIDWithString:PACKET_CHARACTERISTIC_UUID_B]];
        if (isACharacteristic) {
            
            // If it is, subscribe to it
            /*
            NSString *notifyMessage = [NSString stringWithFormat:@"Started Notify Characteristic %@", isACharacteristic ? @"A" : @"B"];
            NSLog(@"%@",notifyMessage);
            [NotificationUtils localNotification:notifyMessage];
            [peripheral setNotifyValue:YES forCharacteristic:characteristic];
             */
            NSLog(@"NOT listening to notifications on FF01 for now");
        } else if (isBCharacteristic) {
            NSLog(@"Going to read from FF02");
            [NotificationUtils localNotification:@"Going to read from FF02"];
            [peripheral readValueForCharacteristic:characteristic];
        }
    }
    
    // Once this is complete, we just need to wait for the data to come in.
}

/** This callback lets us know more data has arrived via notification on the characteristic
 */
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    NSString * _Nonnull characteristicUUID = characteristic.UUID.UUIDString;
    if (error) {
        NSLog(@"Error discovering characteristics: %@", [error localizedDescription]);
        if (self.dataDelegate) {
            [self.dataDelegate onErrorIn:error onCharacteristic:characteristicUUID forDevice:[DeviceModel getDeviceFromRepo:peripheral.identifier.UUIDString]];
        }
        return;
    }
    NSUInteger characteristicLength = characteristic.value.length;

    if (![DeviceModel repoHasDevice:peripheral.identifier.UUIDString]) {
        [DeviceModel addDeviceToRepo:[DeviceModel newPeripheral:peripheral]];
    }
    
    NSLog(@"Received: %@ on %@ (length:%d)", characteristic.value, characteristicUUID, characteristicLength);
    
    NSString *stringFromData = [[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding];
    
    [NotificationUtils localNotification:[NSString stringWithFormat:@"Got Info %@ on %@ (length:%d)", stringFromData, characteristicUUID, characteristicLength]];
    
    if (self.dataDelegate) {
        [self.dataDelegate onNewDataIn:characteristic.value onCharacteristic:characteristicUUID forDevice:[DeviceModel getDeviceFromRepo:peripheral.identifier.UUIDString]];
        //convert data to hex string
        [self.dataDelegate onNewStringIn:[NSString stringWithFormat:@"(DATA) %@", [characteristic.value hexStringRepresentation]] onCharacteristic:characteristicUUID forDevice:[DeviceModel getDeviceFromRepo:peripheral.identifier.UUIDString] debugOnly:YES];
        
        if (stringFromData) {
            [self.dataDelegate onNewStringIn:stringFromData onCharacteristic:characteristicUUID forDevice:[DeviceModel getDeviceFromRepo:peripheral.identifier.UUIDString]];
        }
    }
    
    //check if we got a full read packet
    if ([PACKET_CHARACTERISTIC_UUID_B isEqualToString:characteristicUUID] && characteristicLength == PACKET_LONG_LENGTH) {
        //we did, so disconnect
        NSLog(@"We got a full READ packet from %@ of size %d", characteristicUUID, characteristicLength);
        [NotificationUtils localNotification:[NSString stringWithFormat:@"We got a full READ packet from %@ of size %d", characteristicUUID, characteristicLength]];
        
        [[[[AppDelegate instance] bleController] getManagerDelegate] cleanup:peripheral];
    }
    
//    [peripheral writeValue:[@"987654321" dataUsingEncoding:NSUTF8StringEncoding] forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
//    //    [self.peripheralManager updateValue:[@"EOM" dataUsingEncoding:NSUTF8StringEncoding] forCharacteristic:self.transferCharacteristic onSubscribedCentrals:nil];
//    // Have we got everything we need?
//    //    if ([stringFromData isEqualToString:@"EOM"]) {
//
//    // We have, so show the data,
//    [self.textview setText:[[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding]];
//
//    // Cancel our subscription to the characteristic
//    //        [peripheral setNotifyValue:NO forCharacteristic:characteristic];
//
//    // and disconnect from the peripehral
//    //        [self.centralManager cancelPeripheralConnection:peripheral];
//    //    }
//
//    // Otherwise, just add the data on to what we already have
//    [self.data appendData:characteristic.value];
    
    // Log it
//    NSLog(@"Received: %@", stringFromData);
}


/** The peripheral letting us know whether our subscribe/unsubscribe happened or not
 */
- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (error) {
        NSLog(@"Error changing notification state: %@", error.localizedDescription);
    }
    
    // Exit if it's not the transfer characteristic
    if (![characteristic.UUID isEqual:[CBUUID UUIDWithString:PACKET_CHARACTERISTIC_UUID_A]]) {
        return;
    }
    
    // Notification has started
    if (characteristic.isNotifying) {
        NSLog(@"Notification began on %@", characteristic);
    }
    
    // Notification has stopped
    else {
        // so disconnect from the peripheral
        NSLog(@"Notification stopped on %@.  Disconnecting", characteristic);
        [[[[AppDelegate instance] bleController] getManagerDelegate] cleanup:peripheral];
    }
}


@end
