//
//  BleManager.m
//  Hex
//
//  Created by Shaun Wuth on 2018/08/01.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import "BleManager.h"
#import "BleController.h"
#import "BleControllerPrivate.h"
#import "BleConstants.h"
#import "DeviceModel.h"
#import "NotificationUtils.h"
#import "Log.h"

@interface BleManager()

@property dispatch_queue_t centralQueue;
@property CBCentralManager *centralManager;

@property NSMutableDictionary<NSString*, CBPeripheral *> *discoveredPeripherals;

@property NSObject *peripheralLock;

@end

@implementation BleManager

- (instancetype)init {
    if (self = [super init]) {
        [self setup];
    }
    
    return self;
}

- (void) setup {
    NSLog(@"BleManager Setup");
    self.peripheralLock = [NSObject new];
    
    self.centralQueue = dispatch_queue_create("com.customisedapplications.hex.mycentralqueue", DISPATCH_QUEUE_CONCURRENT);// or however you want to create your dispatch_queue_t
    
    @try {
        self.centralManager = [[CBCentralManager alloc]initWithDelegate:self queue:self.centralQueue options:@{ CBCentralManagerOptionRestoreIdentifierKey: @"mainHexCentralIdentifier", CBCentralManagerOptionShowPowerAlertKey: @(YES) }];
    } @catch (NSException * ex) {
        NSLog(@"Unable to create central manager!");
    }
    
    self.discoveredPeripherals = [NSMutableDictionary<NSString*, CBPeripheral*> new];
    NSLog(@"BleManager Setup Complete");

}

- (void) startScanning {
    if (!self.centralManager) {
        [self setup];
    }
    if (!self.centralManager) {
        return;
    }
    
    [self.centralManager scanForPeripheralsWithServices:@[[CBUUID UUIDWithString:PACKET_SERVICE_UUID]] options:@{ CBCentralManagerScanOptionAllowDuplicatesKey : @YES }];
    
    NSLog(@"BleManager Started Scanning");
}

- (void) stopScanning {
    [self.centralManager stopScan];

    NSLog(@"BleManager Stopped Scanning");
}

- (void) connectToPeripheral: (CBPeripheral *) peripheral {
    @try {
        [self.discoveredPeripherals setObject:peripheral forKey:peripheral.identifier.UUIDString];
        [DeviceModel addDeviceToRepo:[DeviceModel newPeripheral:peripheral]];
        @synchronized(self.peripheralLock) {
            if (peripheral.state != CBPeripheralStateConnected) {
                NSLog(@"BleManager connecting to peripheral: %@", peripheral.identifier.UUIDString);
                [self.centralManager connectPeripheral:peripheral options:nil];
            }
        }
    } @catch (NSException * e) {
        NSLog(@"Failed to connect to peripheral - cleaning up: %@", e);
        [self cleanup:peripheral];
        [self startScanning];
    }
}

- (void) cleanup {
    for (CBPeripheral * discoveredPeripheral in self.discoveredPeripherals.allValues) {
        [self cleanup: discoveredPeripheral];
    }
}

- (void) cleanup: (CBPeripheral *) discoveredPeripheral {
    [DeviceModel removeDeviceFromRepo:discoveredPeripheral.identifier.UUIDString];
    
    @try {
        // Don't do anything if we're not connected
        if (discoveredPeripheral.state != CBPeripheralStateConnected) {
            return;
        }
        
        // See if we are subscribed to a characteristic on the peripheral
        if (discoveredPeripheral.services != nil) {
            for (CBService *service in discoveredPeripheral.services) {
                if (service.characteristics != nil) {
                    for (CBCharacteristic *characteristic in service.characteristics) {
                        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:PACKET_CHARACTERISTIC_UUID_A]]) {
                            if (characteristic.isNotifying) {
                                // It is notifying, so unsubscribe
                                [discoveredPeripheral setNotifyValue:NO forCharacteristic:characteristic];
                                
                                // And we're done.
                                return;
                            }
                        }
                    }
                }
            }
        }
        
        // If we've got this far, we're connected, but we're not subscribed, so we just disconnect
        [self.centralManager cancelPeripheralConnection: discoveredPeripheral];
    } @catch (NSException * e) {
        NSLog(@"Failed to clean up for a peripheral: %@", e);
    }
    
    
}

#pragma mark CBCentralManager

- (void)centralManagerDidUpdateState:(nonnull CBCentralManager *)central {
    
    if (central.state == CBManagerStatePoweredOn) {
        NSLog(@"BleManager Did Update State: CBManagerStatePoweredOn");
        [self startScanning];
        
        if (self.discoveredPeripherals.allValues.count > 0) {
            for (CBPeripheral *peripheral in self.discoveredPeripherals.allValues) {
                peripheral.delegate = [[[AppDelegate instance] bleController] getPeripheralDelegate];
                
                [peripheral discoverServices:@[[CBUUID UUIDWithString:PACKET_SERVICE_UUID]]];

            }
            
        }
    }
    else if ([central state] == CBManagerStateUnauthorized) {
        NSLog(@"BleManager Did Update State: CBManagerStateUnauthorized");
    }
    else if ([central state] == CBManagerStateUnknown) {
        NSLog(@"BleManager Did Update State: CBManagerStateUnknown");
    }
    else if ([central state] == CBManagerStateUnsupported) {
        NSLog(@"BleManager Did Update State: CBManagerStateUnsupported");
    }
    else if ([central state] == CBManagerStatePoweredOff || [central state] == CBManagerStateResetting) {
        NSLog(@"BleManager Did Update State: %@", [central state] == CBManagerStatePoweredOff ? @"CBManagerStatePoweredOff" : @"CBManagerStateResetting");
    }
}

- (void)centralManager:(CBCentralManager *)central willRestoreState:(NSDictionary<NSString *, id> *)state {
    NSLog(@"BleManager Will Restore State: %@", state);
    
    self.centralManager = central;
    
    NSArray *peripherals = state[CBCentralManagerRestoredStatePeripheralsKey];
    
    if (peripherals != nil && peripherals.count > 0) {
        for (CBPeripheral* peripheral in peripherals)
        {
            [self connectToPeripheral:peripheral];
        }
    }
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *, id> *)advertisementData RSSI:(NSNumber *)RSSI {
    
    switch (peripheral.state) {
        case CBPeripheralStateDisconnected:
            NSLog(@"BleManager did discover peripheral: %@ at %@ in state CBPeripheralStateDisconnected", peripheral.name, RSSI);
            break;
        case CBPeripheralStateConnecting:
            NSLog(@"BleManager did discover peripheral: %@ at %@ in state CBPeripheralStateConnecting", peripheral.name, RSSI);
            break;
        case CBPeripheralStateDisconnecting:
            NSLog(@"BleManager did discover peripheral: %@ at %@ in state CBPeripheralStateDisconnecting", peripheral.name, RSSI);
            break;
        default:
            NSLog(@"BleManager did discover peripheral: %@ at %@ in state CBPeripheralStateConnected", peripheral.name, RSSI);
            break;
    }

    [self connectToPeripheral:peripheral];
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"BleManager did connect peripheral: %@", peripheral.name);
    
    [NotificationUtils localNotification:@"BleManager did connect peripheral"];
    
    [Log addBleConnectLog:peripheral.identifier.UUIDString];
    
    peripheral.delegate = [[[AppDelegate instance] bleController] getPeripheralDelegate];
    
    [peripheral discoverServices:nil];
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(nullable NSError *)error {
    NSLog(@"BleManager did FAIL to connect peripheral: %@ with error %@", peripheral.name, error);
    [NotificationUtils localNotification:@"BleManager did NOT connect peripheral"];
    [self cleanup: peripheral];
    [self startScanning];
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(nullable NSError *)error {
    NSLog(@"BleManager did disconnect peripheral: %@ with error %@", peripheral.name, error);
    [NotificationUtils localNotification:@"BleManager did disconnect peripheral"];
    [self cleanup: peripheral];
}

#pragma mark BeaconManagerDelegate

- (void)didEnterRegion {
    [self cleanup];
    [self startScanning];
}

- (void)didExitRegion {
    
}

@end
