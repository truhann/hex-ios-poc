//
//  BleControllerPrivate.h
//  Hex
//
//  Created by Shaun Wuth on 2018/08/01.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#ifndef BleControllerPrivate_h
#define BleControllerPrivate_h

#import "BlePeripheral.h"
#import "BleManager.h"

@interface BleController (Private)

- (BlePeripheral *) getPeripheralDelegate;
- (BleManager *) getManagerDelegate;

@end

#endif /* BleControllerPrivate_h */
