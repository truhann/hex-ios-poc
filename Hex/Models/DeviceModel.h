	//
//  DeviceModel.h
//  Hex
//
//  Created by Shaun Wuth on 2018/08/06.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface DeviceModel : NSObject

@property NSUUID * uuid;
@property CBPeripheral *peripheral;

+ (instancetype) repo;

+ (void) addDeviceToRepo:(DeviceModel *) device;
+ (DeviceModel *) getDeviceFromRepo:(NSString *) deviceId;
+ (BOOL) repoHasDevice:(NSString *) deviceId;
+ (DeviceModel *) removeDeviceFromRepo:(NSString *) deviceId;

+ (instancetype) newPeripheral:(CBPeripheral*) peripheral;

- (instancetype) initWithPeripheral:(CBPeripheral*) peripheral;

@end
