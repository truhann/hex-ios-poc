//
//  PersistenceManager.m
//  Hex
//
//  Created by Shaun Wuth on 2018/08/09.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import "PersistenceManager.h"
#import <Realm/Realm.h>

@implementation PersistenceManager

- (instancetype)init {
    if (self = [super init]) {
        [self setup];
    }
    return self;
}

- (void) setup {
    RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];
    
    // Use the default directory, but replace the filename with the username
    config.fileURL = [[[config.fileURL URLByDeletingLastPathComponent]
                       URLByAppendingPathComponent:@"logs"]
                      URLByAppendingPathExtension:@"realm"];
    
    [RLMRealmConfiguration setDefaultConfiguration:config];
}

@end
