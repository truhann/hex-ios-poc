//
//  Log.m
//  Hex
//
//  Created by Shaun Wuth on 2018/08/09.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import "Log.h"
#import "DateUtils.h"
#import "DeviceIdentifier.h"

@implementation Log

- (NSString *)getFullMessage {
    return [NSString stringWithFormat:@"%@ - %@", [DateUtils stringFromDate:self.createdAt], self.message];
}

- (NSString *) getServerMessage {
    return [NSString stringWithFormat:@"%@ | %@ | %@ | %@",
            [DateUtils stringFromDate:self.createdAt],
            [NSString stringWithFormat:@"%@ (%@)", [[UIDevice currentDevice] name], [DeviceIdentifier id]],
            self.title,
            self.message];
}

+ (Log *)addLog:(NSString *)title message:(NSString *)message {
    Log *log = [Log new];
    log.title = title;
    log.message = message;
    log.createdAt = [NSDate date];
    log.id = [NSUUID UUID].UUIDString;
    
    [[RLMRealm defaultRealm] transactionWithBlock:^{
        [[RLMRealm defaultRealm] addObject:log];
    }];
    
    return log;
}

+ (Log *)addEnterLog {
    return [Log addLog:@"Entered Region" message:@""];
}

+ (Log *)addExitLog {
    return [Log addLog:@"Exited Region" message:@""];
}

+ (Log *)addBleConnectLog:(NSString *)deviceId {
    return [Log addLog:@"Connected to BLE" message:[NSString stringWithFormat:@"Device: %@", deviceId]];
}

+ (Log *)addBleMessageLog:(NSString *)message forDevice:(NSString *)deviceId {
    return [Log addLog:@"BLE Message" message:[NSString stringWithFormat:@"Device: %@, message: %@", deviceId, message]];
}

+ (Log *)addErrorLog:(NSString *)errorMessage forDevice:(NSString *)deviceId {
    return [Log addLog:@"BLE Error" message:[NSString stringWithFormat:@"Device: %@, error: %@", deviceId, errorMessage]];
}

@end
