//
//  Log.h
//  Hex
//
//  Created by Shaun Wuth on 2018/08/09.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Realm/Realm.h>

@interface Log : RLMObject

@property NSString * _Nonnull id;
@property NSDate * _Nonnull createdAt;
@property NSString * _Nonnull title;
@property NSString * _Nonnull message;
@property BOOL uploadedToServer;
@property BOOL uploadedToDebugServer;

+ (Log * _Nullable) addLog:(NSString* _Nonnull) title
        message:(NSString* _Nonnull) message;

+ (Log * _Nullable) addEnterLog;
+ (Log * _Nullable) addExitLog;
+ (Log * _Nullable) addBleMessageLog:(NSString* _Nonnull) message forDevice:(NSString* _Nonnull) deviceId;
+ (Log * _Nullable) addErrorLog:(NSString* _Nonnull) errorMessage forDevice:(NSString* _Nonnull) deviceId;
+ (Log * _Nullable) addBleConnectLog:(NSString* _Nonnull) deviceId;

- (NSString* _Nonnull) getFullMessage;
- (NSString *) getServerMessage;

@end

RLM_ARRAY_TYPE(Log)
