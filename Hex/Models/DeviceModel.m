//
//  DeviceModel.m
//  Hex
//
//  Created by Shaun Wuth on 2018/08/06.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import "DeviceModel.h"

@interface DeviceModel()

@property NSMutableDictionary<NSString*, DeviceModel*> *models;

@end

@implementation DeviceModel

+ (instancetype)repo {
    static DeviceModel *_repo = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _repo = [DeviceModel new];
        _repo.models = [NSMutableDictionary<NSString*, DeviceModel*> dictionary];
    });
    return _repo;
}

+ (void)addDeviceToRepo:(DeviceModel *)device {
    if (device && device.uuid) {
        [[DeviceModel repo].models setObject:device forKey:device.uuid.UUIDString];
    } else {
        NSLog(@"Not adding to repo!");
    }
}

+ (DeviceModel *)getDeviceFromRepo:(NSString *)deviceId {
    return [[DeviceModel repo].models objectForKey:deviceId];
}

+ (BOOL)repoHasDevice:(NSString *)deviceId {
    if (!deviceId || ![DeviceModel getDeviceFromRepo:deviceId]) {
        return [DeviceModel getDeviceFromRepo:deviceId] != nil;
    } else return false;
}

+ (DeviceModel *)removeDeviceFromRepo:(NSString *)deviceId {
    DeviceModel *toRemove = [DeviceModel getDeviceFromRepo:deviceId];
    [[DeviceModel repo].models removeObjectForKey:deviceId];
    
    return toRemove;
}

+ (instancetype)newPeripheral:(CBPeripheral *)peripheral {
    return [[DeviceModel alloc] initWithPeripheral:peripheral];
}

- (instancetype)initWithPeripheral:(CBPeripheral *)peripheral {
    if (self = [self init]) {
        self.uuid = peripheral.identifier;
        self.peripheral = peripheral;
    }
    
    return self;
}

@end
