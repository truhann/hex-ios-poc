//
//  BeaconManager.m
//  Hex
//
//  Created by Shaun Wuth on 2018/08/06.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import "BeaconManager.h"
#import "DataTypeUtils.h"
#import "DataHandler.h"

@interface BeaconManager()

@property CLLocationManager *locationManager;
@property CLBeaconRegion *beaconRegion;
@property id<DataDelegate> dataDelegate;

@end

@implementation BeaconManager

- (instancetype) init {
    if (self = [super init]) {
        [self setup];
    }
    
    return self;
}

- (void) setup {
    self.locationManager = [CLLocationManager new];
    [self.locationManager requestAlwaysAuthorization];
    self.locationManager.allowsBackgroundLocationUpdates = YES;
//    self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.headingFilter = kCLHeadingFilterNone;
    
    [self setRegion];
    
    self.locationManager.delegate = self;
    [self.locationManager startMonitoringForRegion:self.beaconRegion];
    
    self.dataDelegate = [DataHandler new];
}

- (void) setRegion {
    NSString *uuidString = @"ADA50693-A4E2-4FB1-AFCF-C6EB07647825";
    NSString *majorString = @"4EC7";
    NSString *minorString = @"55C6";
    
    //Convert HEX Major to Int
    CLBeaconMajorValue majorInt = [DataTypeUtils hexStringToInt:majorString];
    CLBeaconMinorValue minorInt = [DataTypeUtils hexStringToInt:minorString];
    
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:uuidString];
    
    //Define the region
    self.beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid major:majorInt minor:minorInt identifier:@"HexWakeUpBeacon"];
}

#pragma mark CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region {
    
    switch (state) {
        case CLRegionStateInside:
            [self.dataDelegate onEnterRegion];
            [self.locationManager startRangingBeaconsInRegion: self.beaconRegion];
            if (self.delegate) {
                [self.delegate didEnterRegion];
            }
            break;
        case CLRegionStateOutside:
            [self.dataDelegate onExitRegion];
            if (self.delegate) {
                [self.delegate didExitRegion];
            }
            break;

        default:
            NSLog(@"State Change Unknown");
            break;
    }
}

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray<CLBeacon *> *)beacons inRegion:(CLBeaconRegion *)region {
//    if(beacons.count > 0){
//        print("RANGED \(beacons.count) BEACONS")
//        if((beacons[0].proximity == .immediate && range == 0) || (beacons[0].proximity == .near && range == 1) || (beacons[0].proximity == .far && range == 2)){
//            let notification = UILocalNotification()
//            notification.alertBody = msg
//            notification.soundName = "Default"
//            UIApplication.shared.scheduleLocalNotification(notification)
//            //After notification is sent, stop ranging and keep monitoring.
//            locationManager!.stopRangingBeacons(in: beaconRegion)
//        }
//    }
}

@end
