//
//  BeaconManager.h
//  Hex
//
//  Created by Shaun Wuth on 2018/08/06.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol BeaconManagerDelegate

- (void) didEnterRegion;
- (void) didExitRegion;

@end

@interface BeaconManager : NSObject<CLLocationManagerDelegate>

@property id<BeaconManagerDelegate> delegate;

@end
