//
//  AppDelegate.h
//  Hex
//
//  Created by Shaun Wuth on 2018/08/01.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BleController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

+ (instancetype) instance;
- (BleController*) bleController;

@end

