//
//  ViewController.m
//  Hex
//
//  Created by Shaun Wuth on 2018/08/01.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = self.logTitle;
    self.textView.text = self.logMessage;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
