//
//  LogTableViewController.m
//  Hex
//
//  Created by Shaun Wuth on 2018/08/09.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import "LogTableViewController.h"
#import "Log.h"
#import "DeviceIdentifier.h"
#import "ViewController.h"

@interface LogTableViewController ()

@property RLMResults<Log*> *logs;
@property RLMNotificationToken *notificationToken;

@end

@implementation LogTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 32)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(8, 6, self.view.frame.size.width - 16, 20)];
    label.text = [NSString stringWithFormat:@"%@ (%@)", [[UIDevice currentDevice] name], [DeviceIdentifier id]];
    label.font = [UIFont systemFontOfSize:9];
    label.textColor = [UIColor redColor];
    [view addSubview:label];
    
    self.tableView.tableHeaderView = view;
    
    self.logs = [[Log allObjects] sortedResultsUsingKeyPath:@"createdAt" ascending:NO];
    
    // Observe RLMResults Notifications
    __weak typeof(self) weakSelf = self;
    self.notificationToken = [[Log allObjects]
                              addNotificationBlock:^(RLMResults<Log *> *results, RLMCollectionChange *changes, NSError *error) {
                                  
                                  if (error) {
                                      NSLog(@"Failed to open Realm on background worker: %@", error);
                                      return;
                                  }
                                  
                                  UITableView *tableView = weakSelf.tableView;
                                  // Initial run of the query will pass nil for the change information
//                                  if (!changes) {
                                      [tableView reloadData];
//                                      return;
//                                  }
//
//                                  // Query results have changed, so apply them to the UITableView
//                                  [tableView beginUpdates];
//                                  [tableView deleteRowsAtIndexPaths:[changes deletionsInSection:0]
//                                                   withRowAnimation:UITableViewRowAnimationAutomatic];
//                                  [tableView insertRowsAtIndexPaths:[changes insertionsInSection:0]
//                                                   withRowAnimation:UITableViewRowAnimationAutomatic];
//                                  [tableView reloadRowsAtIndexPaths:[changes modificationsInSection:0]
//                                                   withRowAnimation:UITableViewRowAnimationAutomatic];
//                                  [tableView endUpdates];
//
//                                  [tableView setNeedsLayout];
                              }];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.logs ? self.logs.count : 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    // Configure the cell...
    Log * log = [self.logs objectAtIndex:indexPath.row];
    cell.textLabel.text = log.title;
    cell.detailTextLabel.text = [log getFullMessage];
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//     Get the new view controller using [segue destinationViewController].
//     Pass the selected object to the new view controller.
    if ([[segue destinationViewController] isKindOfClass:[ViewController class]]) {
        ((ViewController*)[segue destinationViewController]).logMessage = ((UITableViewCell*)sender).detailTextLabel.text;
        ((ViewController*)[segue destinationViewController]).logTitle = ((UITableViewCell*)sender).textLabel.text;
    }
}


@end
