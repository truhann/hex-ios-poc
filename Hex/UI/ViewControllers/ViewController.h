//
//  ViewController.h
//  Hex
//
//  Created by Shaun Wuth on 2018/08/01.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property NSString *logMessage;
@property NSString *logTitle;

@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

