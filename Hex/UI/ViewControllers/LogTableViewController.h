//
//  LogTableViewController.h
//  Hex
//
//  Created by Shaun Wuth on 2018/08/09.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogTableViewController : UITableViewController

@end
