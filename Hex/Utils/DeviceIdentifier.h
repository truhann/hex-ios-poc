//
//  DeviceIdentifier.h
//  Hex
//
//  Created by Shaun Wuth on 2018/08/10.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeviceIdentifier : NSObject

+ (NSString* _Nonnull) id;

@end
