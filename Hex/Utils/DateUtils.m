//
//  DateUtils.m
//  Hex
//
//  Created by Shaun Wuth on 2018/08/10.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import "DateUtils.h"

@implementation DateUtils

+ (NSDateFormatter*) formatter {
    static NSDateFormatter *_formatter = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _formatter = [NSDateFormatter new];
        _formatter.dateFormat = @"ddMMM-HH:mm:ss";
    });
    return _formatter;
}

+ (NSString*) stringFromDate:(NSDate*) date {
    return [[DateUtils formatter] stringFromDate:date];
}

@end
