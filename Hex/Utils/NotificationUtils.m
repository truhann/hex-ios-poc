//
//  NotificationUtils.m
//  Hex
//
//  Created by Shaun Wuth on 2018/08/06.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import "NotificationUtils.h"
#import <UIKit/UIKit.h>

@implementation NotificationUtils

+ (void) localNotification: (NSString*) message {
    dispatch_async(dispatch_get_main_queue(), ^{
        UILocalNotification *notification = [UILocalNotification new];
        notification.soundName = @"Default";
        notification.alertBody = message;
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    });
}

@end
