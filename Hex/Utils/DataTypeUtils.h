//
//  DataTypeUtils.h
//  Hex
//
//  Created by Shaun Wuth on 2018/08/06.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataTypeUtils : NSObject

+(unsigned int)hexStringToInt:(NSString*)hexString;

@end
