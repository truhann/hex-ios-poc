//
//  DateUtils.h
//  Hex
//
//  Created by Shaun Wuth on 2018/08/10.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateUtils : NSObject

+ (NSString*) stringFromDate:(NSDate*) date;

@end
