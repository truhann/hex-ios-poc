//
//  NotificationUtils.h
//  Hex
//
//  Created by Shaun Wuth on 2018/08/06.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotificationUtils : NSObject

+ (void) localNotification: (NSString*) message;

@end
