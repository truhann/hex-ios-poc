//
//  DeviceIdentifier.m
//  Hex
//
//  Created by Shaun Wuth on 2018/08/10.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import "DeviceIdentifier.h"
#import <AdSupport/AdSupport.h>

#define UUID_ZEROS @"00000000-0000-0000-0000-000000000000"

@implementation DeviceIdentifier

+ (NSString *)id {
    NSString * toReturn;
    
    if ([[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled]) {
        toReturn = [[ASIdentifierManager sharedManager] advertisingIdentifier].UUIDString;
    }
    
    if (!toReturn || [[toReturn substringToIndex:4] isEqualToString:UUID_ZEROS]) {
        toReturn = [UIDevice currentDevice].identifierForVendor.UUIDString;
    }
    
    if (!toReturn || [[toReturn substringToIndex:4] isEqualToString:UUID_ZEROS]) {
        toReturn = [NSUserDefaults.standardUserDefaults stringForKey:@"TheId"];
        if (!toReturn) {
            toReturn = [NSUUID UUID].UUIDString;
            [NSUserDefaults.standardUserDefaults setObject:toReturn forKey:@"TheId"];
        }
    }
    
    return toReturn;
}

@end
