//
//  DataTypeUtils.m
//  Hex
//
//  Created by Shaun Wuth on 2018/08/06.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import "DataTypeUtils.h"

@implementation DataTypeUtils

+(unsigned int)hexStringToInt:(NSString*)hexString
{
    unsigned int result = 0;
    NSScanner *scanner = [NSScanner scannerWithString:[hexString stringByReplacingOccurrencesOfString:@" " withString:@""]];
    [scanner scanHexInt:&result];
    return result;
}

@end
