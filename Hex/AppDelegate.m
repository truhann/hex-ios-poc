//
//  AppDelegate.m
//  Hex
//
//  Created by Shaun Wuth on 2018/08/01.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import "AppDelegate.h"
#import "BleController.h"
#import "BeaconManager.h"
#import "BleControllerPrivate.h"
#import "PersistenceManager.h"

@interface AppDelegate ()

@property BleController *bleController;
@property BeaconManager *beaconManager;
@property PersistenceManager *persistanceManager;

@end

@implementation AppDelegate

+ (instancetype)instance {
    __block AppDelegate *toReturn;
    dispatch_sync(dispatch_get_main_queue(), ^{
        toReturn = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    });
    
    return toReturn;
}

- (BleController *) getBleController {
    return self.bleController;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    //Request authorization for notifications used for background monitoring
    UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeSound | UIUserNotificationTypeAlert categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    
    self.persistanceManager = [PersistenceManager new];
    
    self.bleController = [BleController new];
    self.beaconManager = [BeaconManager new];
    self.beaconManager.delegate = [self.bleController getManagerDelegate];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
