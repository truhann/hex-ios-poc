//
//  DataHandler.m
//  Hex
//
//  Created by Shaun Wuth on 2018/08/01.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import "DataHandler.h"
#import "DebugServer.h"
#import "RealServer.h"
#import "NotificationUtils.h"
#import "Log.h"

@interface DataHandler()

@property id<ServerProtocol> debugServer;
@property id<ServerProtocol> realServer;

@end

@implementation DataHandler

- (instancetype)init {
    if (self = [super init]) {
        [self setup];
    }
    return self;
}

- (void) setup {
    self.debugServer = [DebugServer new];
    self.realServer = [RealServer new];
}

- (void) onEnterRegion {
    NSLog(@"Did Enter Region");
    [NotificationUtils localNotification:@"Entered Region"];
    Log *log = [Log addEnterLog];
    if (self.debugServer/* && [self.debugServer respondsToSelector:@selector(uploadLogToServer:)]*/) {
        [self.debugServer uploadLogToServer:log];
    }
}

- (void) onExitRegion {
    NSLog(@"Did Exit Region");
    [NotificationUtils localNotification:@"Exited Region"];
    Log* log = [Log addExitLog];
    if (self.debugServer/* && [self.debugServer respondsToSelector:@selector(uploadLogToServer:)]*/) {
        [self.debugServer uploadLogToServer:log];
    }
}

- (void)onErrorIn:(NSError *)error onCharacteristic:(NSString *)characteristicName forDevice:(DeviceModel *) device {
    NSLog(@"Got an error on %@: %@", characteristicName, error);
    
    [Log addErrorLog:error.debugDescription forDevice:device.uuid.UUIDString];
    
    if (self.debugServer) {
        [self.debugServer uploadStringToServer:[NSString stringWithFormat:@"Error on characteristic %@: %@", characteristicName, error] forDevice:device];
    }
    if (self.realServer) {
        [self.realServer uploadStringToServer:[NSString stringWithFormat:@"Error on characteristic %@: %@", characteristicName, error] forDevice:device];
    }
}

- (void)onNewDataIn:(NSData *)data onCharacteristic:(NSString *)characteristicName forDevice:(DeviceModel *) device {
    NSLog(@"Got data on %@: %@", characteristicName, data);
    
//    [Log addBleMessageLog:<#(NSString * _Nonnull)#> forDevice:<#(NSString * _Nonnull)#>]
    
    if (self.debugServer) {
        [self.debugServer uploadDataToServer:data forDevice:device];
    }
    if (self.realServer) {
        [self.realServer uploadDataToServer:data forDevice:device];
    }
}

- (void)onNewStringIn:(NSString *)string onCharacteristic:(NSString *)characteristicName forDevice:(DeviceModel *) device {
    [self onNewStringIn:string onCharacteristic:characteristicName forDevice:device debugOnly:NO];
}

- (void)onNewStringIn:(NSString *)string onCharacteristic:(NSString *)characteristicName forDevice:(DeviceModel *) device debugOnly:(BOOL)debugOnly {
    NSLog(@"Got string on %@: %@", characteristicName, string);
    
    [Log addBleMessageLog:string forDevice:device.uuid.UUIDString];
    
    if (self.debugServer) {
        [self.debugServer uploadStringToServer:string forDevice:device];
    }
    if (!debugOnly && self.realServer) {
        [self.realServer uploadStringToServer:string forDevice:device];
    }
}

@end
