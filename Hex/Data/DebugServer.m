//
//  DebugServer.m
//  Hex
//
//  Created by Shaun Wuth on 2018/08/06.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import "DebugServer.h"
#import <AFNetworking/AFNetworking.h>
#import "DeviceIdentifier.h"
#import "DateUtils.h"
#import "Log.h"

@implementation DebugServer

- (void)uploadDataToServer:(NSData *)data forDevice:(DeviceModel *) device {
    NSLog(@"Uploading data to debug server %@: %@", device.uuid, data);
    
    NSURL *URL = [NSURL URLWithString:@"hex.php" relativeToURL:[NSURL URLWithString:@"http://lattech.co.za"]];
    
    //para
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    [manager POST:URL.absoluteString parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
//        formData
//    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        NSLog(@"Success Upload to Debug: %@", responseObject);
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        NSLog(@"Failure Upload to Debug: %@", error);
//    }];
    [manager POST:URL.absoluteString parameters:data progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"Success Upload to Debug: %@", responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Failure Upload to Debug: %@", error);
    }];
}

- (void)uploadStringToServer:(NSString *)string forDevice:(DeviceModel *) device {
    NSLog(@"Uploading string to debug server %@: %@", device.uuid, string);
    
    NSURL *URL = [NSURL URLWithString:@"hex.php" relativeToURL:[NSURL URLWithString:@"http://lattech.co.za"]];
    
    NSString *toSend = [NSString stringWithFormat:@"String: %@ | %@ | %@ | %@",
                        [DateUtils stringFromDate:[NSDate date]],
                        [NSString stringWithFormat:@"%@ (%@)", [[UIDevice currentDevice] name],[DeviceIdentifier id]],
                        device.uuid.UUIDString,
                        string];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:URL.absoluteString parameters:toSend progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"Success Upload to Debug: %@", responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Failure Upload to Debug: %@", error);
        [Log addLog:@"Upload Failure" message:[NSString stringWithFormat:@"to Debug: %@", error]];
    }];
    
}

- (void)uploadLogToServer:(Log *)log {
    NSLog(@"Uploading log to debug server: %@", log.message);
    
    NSURL *URL = [NSURL URLWithString:@"hex.php" relativeToURL:[NSURL URLWithString:@"http://lattech.co.za"]];
    
    NSString *toSend = [log getServerMessage];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:URL.absoluteString parameters:toSend progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"Success Upload LOG to Debug: %@, %@", log.message, responseObject);
        [RLMRealm.defaultRealm transactionWithBlock:^{
            log.uploadedToDebugServer = YES;
        }];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Failure Upload LOG to Debug: %@, %@", log.message, error);
        [Log addLog:@"Upload Failure" message:[NSString stringWithFormat:@"to Debug: %@, %@", log.message, error]];
    }];
}

@end
