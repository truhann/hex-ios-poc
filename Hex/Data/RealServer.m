//
//  RealServer.m
//  Hex
//
//  Created by Shaun Wuth on 2018/08/06.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import "RealServer.h"

@implementation RealServer

- (void)uploadDataToServer:(NSData *)data forDevice:(DeviceModel *) device {
    NSLog(@"Uploading data to real server %@: %@", device.uuid, data);
}

- (void)uploadStringToServer:(NSString *)string forDevice:(DeviceModel *) device {
    NSLog(@"Uploading string to real server %@: %@", device.uuid, string);
}

@end
