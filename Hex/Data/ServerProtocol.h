//
//  ServerProtocol.h
//  Hex
//
//  Created by Shaun Wuth on 2018/08/06.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#ifndef ServerProtocol_h
#define ServerProtocol_h

#import <Foundation/Foundation.h>
#import "DeviceModel.h"
#import "Log.h"

@protocol ServerProtocol

@optional

- (void) uploadStringToServer:(NSString* ) string forDevice:(DeviceModel *) device;
- (void) uploadDataToServer:(NSData* ) data forDevice:(DeviceModel *) device;
- (void) uploadLogToServer:(Log *) log;

@end

#endif /* ServerProtocol_h */
