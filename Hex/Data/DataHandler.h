//
//  DataHandler.h
//  Hex
//
//  Created by Shaun Wuth on 2018/08/01.
//  Copyright © 2018 Customised Applications. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DeviceModel.h"

@protocol DataDelegate

- (void) onEnterRegion;
- (void) onExitRegion;

@optional

- (void) onNewDataIn:(NSData *) data onCharacteristic:(NSString*) characteristicName forDevice:(DeviceModel *) device;
- (void) onNewStringIn:(NSString *) string onCharacteristic:(NSString*) characteristicName forDevice:(DeviceModel *) device;
- (void) onNewStringIn:(NSString *) string onCharacteristic:(NSString*) characteristicName forDevice:(DeviceModel *) device debugOnly:(BOOL) debugOnly;
- (void) onErrorIn:(NSError *) error onCharacteristic:(NSString*) characteristicName forDevice:(DeviceModel *) device;

@end

@interface DataHandler : NSObject<DataDelegate>

@end
